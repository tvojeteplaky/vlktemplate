$(document).foundation();

var nowTemp = new Date();
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
var checkin = $('#contactOd').fdatepicker({
	format: 'dd. mm. yyyy',
	language: 'cs',
	onRender: function (date) {
		return date.valueOf() < now.valueOf() ? 'disabled' : '';
	}
}).on('changeDate', function (ev) {
	if (ev.date.valueOf() > checkout.date.valueOf()) {
		var newDate = new Date(ev.date)
		newDate.setDate(newDate.getDate() + 1);
		checkout.update(newDate);
	}
	checkin.hide();
	$('#contactDo')[0].focus();
}).data('datepicker');
var checkout = $('#contactDo').fdatepicker({
	language: 'cs',
	format: 'dd. mm. yyyy',
	onRender: function (date) {
		return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
	}
}).on('changeDate', function (ev) {
	checkout.hide();
}).data('datepicker');



$(document).ready(function() {

	if(!!$("[data-product-item]")) {
		// $(document).on('closeme.zf.reveal',"[data-product-item]",function(ev){
		// 	window.location.hash = this.id;
		// });
		$(document).on('closed.zf.reveal',"[data-product-item]",function(ev){
			window.location.hash = "";
		});
	}

	if(!!window.location.hash && !!window.location.hash.replace("#","")) {
		var el =window.location.hash;
		if(!!$(el).length){
			$(el).foundation("open");
		}  else if(el.search("detailItem")) {
			$.get("",{h_module:"catalogue",h_action:"detail",h_secure:"2e879e1fe276d17d34d78391661753c2",product_id:el.replace("#detailItem-","")}, function(data) {
				data = JSON.parse(data);
				$("#page").append(data.main_content);
				$(el).foundation();
				$(el).foundation("open");
				// console.log(data.main_content);
			});
			
		 	// console.log("není tu");
		}
	}
	if (!!$("#revealMessage").length) {
		$("#revealMessage").foundation("open");
		setTimeout(function() {
			$("#revealMessage").foundation("destroy");
		}, 4000);
	}
	if ($("#owlOffer").length) {
		console.log("Owl is running")
		var owlOffer = $("#owlOffer");

		owlOffer.owlCarousel({
			itemsCustom: [
				[0, 1],
				[700, 2],
				[1000, 3],
				[1300, 4],
				[1600, 5]
			],
			navigation: true,
			navigationText: false,
			autoPlay: 5000,
			stopOnHover: true,
			pagination: false
		});
	}
	if ($("#owlOrder").length) {
		var owlOrder = $("#owlOrder");
		owlOrder.owlCarousel({
			itemsCustom: [
				[0, 1],
				[360, 2],
				[500, 3],
				[640, 4]
			],
			navigation: true,
			navigationText: false,
			autoPlay: 5000,
			stopOnHover: true,
			pagination: false
		});
	}
	if ($("#backToTop").length) {
		console.log("Back to top is running.");
		$(function() {
			$.scrollUp({
				scrollName: 'backToTop', // Element ID
				scrollDistance: 300, // Distance from top/bottom before showing element (px)
				scrollFrom: 'top', // 'top' or 'bottom'
				scrollSpeed: 300, // Speed back to top (ms)
				easingType: 'linear', // Scroll to top easing (see http://easings.net/)
				animation: 'fade', // Fade, slide, none
				animationSpeed: 200, // Animation speed (ms)
				scrollTrigger: false, // Set a custom triggering element. Can be an HTML string or jQuery object
				scrollTarget: false, // Set a custom target element for scrolling to. Can be element or number
				scrollText: '', // Text for element, can contain HTML
				scrollTitle: false, // Set a custom <a> title if required.
				scrollImg: true, // Set true to use image
				activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
				zIndex: 2147483647 // Z-Index for the overlay
			});
		});
	}

	var top = 0;
	$("[data-responsive-toggle]").click(function() {
		if ($("[data-responsive-toggle].toggle").length) {
			console.log("close");
			$(this).toggleClass("toggle");
			$('.mainNav').foundation('hideAll');
			$("#topBar").css("height", "81px");
			$("#underTopBar").css("display", "block");
			$("#topBar").css("min-height", "initial");
			$("#topBar .top-bar").css("min-height", "initial");
			if (top > 0) {
				$("#topBar > nav").removeClass("sticky is-stuck is-anchored");
				$("#topBar > nav").addClass("sticky is-stuck");
			} else {
				$("#topBar > nav").removeClass("sticky is-stuck is-anchored");
				$("#topBar > nav").addClass("sticky is-anchored");
			}
			$(window).scrollTop(top);
		} else {
			console.log("open");
			$(this).toggleClass("toggle");
			top = $(window).scrollTop();
			console.log("Y:" + top);
			$(window).scrollTop(0);
			$("#topBar > nav").removeClass("sticky is-stuck is-anchored");
			$("#topBar .top-bar").css("min-height", "100vh");
			$("#underTopBar").css("display", "none");
			$("#topBar").css("min-height", "100vh");
		}
	});
	$(".top-bar-right .down").click(function() {
		window.location.href = this.attr("href");
	});
	$(".top-bar-right .down").mouseenter(function() {
		console.log("menu down")
		$(this).addClass("is-down");
		$('#submenu').foundation('down', $("#sumbmenucontent"));
	});
	$(".top-bar-right .down").mouseleave(function() {
		console.log("menu up")
		$(this).removeClass("is-down");
		$('#submenu').foundation('up', $("#sumbmenucontent"));
	});

	/*if ($(window).width() < 1024) {
		var top = 0;
		var menu = $('[data-responsive-menu]');
		$('[data-responsive-menu]').on('toggled.zf.responsiveNavigation', function() {
			top = $(window).scrollTop();
			console.log(top);
			$(window).scrollTop(0);
			$("#topBar > nav").removeClass("sticky");
		});
		/!*menu.on('up.zf.accordionMenu', function() {
			console.log(top);
			$(window).scrollTop(top);
		});*!/
	}*/
});

function scrollToAnchor(aid, offset) {
	var aTag = $("div[id='" + aid + "']");
	$('html,body').animate({ scrollTop: aTag.offset().top + offset }, 'slow');
}

function loadProducts(el, target) {
	$.get(el.attr("href"), function(data) {
		var content = JSON.parse(data);
		$(target).append(content.main_content.products);
		if (!!content.main_content.next_page) {
			el.attr("href", content.main_content.next_page);
		} else {
			el.attr("href", "");
			el.addClass("hide");
		}
		$(target).foundation();
	});
}

$(document).ready(function() {
	$(".poptat-sluzbu").click(function() {
		scrollToAnchor('contact-box', -100);
	});

	$("#getNext").click(function(ev) {
		ev.preventDefault();
		loadProducts($(this), ".itemList");

	});

	$(".removeItem").click(function(ev) {
		ev.preventDefault();
		var id = $(this).data("id");
		$("#removeItem").find("[name=product_id]").val(id);
		$("#removeItem").submit();
	});
});


function enableScrollingWithMouseWheel() {
	var el = $('#map');
	if (el.length > 0) {
		map.setOptions({
			scrollwheel: true
		});
	}
}

function disableScrollingWithMouseWheel() {
	var el = $('#map');
	if (el.length > 0 && map.length > 0) {
		map.setOptions({
			scrollwheel: false
		});
	}
}

function initMap() {
	var el = $('#map');
	var myLatLng = window.address;
	map = new google.maps.Map(el[0], {
		zoom: 15,
		center: myLatLng,
		scrollwheel: false, // disableScrollingWithMouseWheel as default
		mapTypeId: google.maps.MapTypeId.ROADMAP
	});

	if (el.length > 0) {
		google.maps.event.addListener(map, 'mousedown', function() {
			enableScrollingWithMouseWheel()
		});
	}
	var marker = new google.maps.Marker({
		position: myLatLng,
		map: map,
		title: 'Půjčovna nářadí Vlk s.r.o.'
	});
}

$(document).ready(function() {
	$('body').on('mousedown', function(event) {
		var clickedInsideMap = $(event.target).parents('#map').length > 0;
		if (!clickedInsideMap) {
			disableScrollingWithMouseWheel();
		}
	});
	$(window).scroll(function() {
		disableScrollingWithMouseWheel();
	});
});
